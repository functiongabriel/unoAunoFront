## Getting Started

First, run the development server:

```bash
yarn install
yarn dev
```
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
## Variables de `.env.local`:

- `NEXT_PUBLIC_API_URL`: Url del Servicio Rest API Example: NEXT_PUBLIC_API_URL="http://localhost:3333"

## Edición de la página:

Puedes comenzar a editar la página modificando `app/page.tsx`. La página se actualiza automáticamente a medida que editas el archivo. Se usan desde componentes de servidor hasta de clientes.
